import RedisClient, {Redis} from 'ioredis';
import {pathToRegexp} from 'path-to-regexp';
import {StreamChannelBroker, Payload} from 'redis-streams-broker';
import {v1 as uuid} from 'uuid';
import Redlock, {Lock} from 'redlock';

// not exposed for some reason
type IoRedisStatus =
  | 'wait'
  | 'reconnecting'
  | 'connecting'
  | 'connect'
  | 'ready'
  | 'close'
  | 'end';

type RedisConstructor = {
  host: string,
  port: number,
  prefix: string,
  config?: any
}

export class RedisConnector {
  prefix: string;
  prefixPattern: RegExp;
  config: any;
  client: Redis;
  brokerClient: Redis;
  subscriber: Redis;
  redlock: Redlock;

  constructor({host, port, prefix, config}: RedisConstructor) {
    this.prefix = prefix;
    this.prefixPattern = new RegExp('^' + prefix);

    // ioredis connects automatically and throws on connection error
    this.client = new RedisClient({host, port, keyPrefix: prefix});
    this.subscriber = new RedisClient({host, port});
    this.brokerClient = new RedisClient({host, port});

    this.redlock = new Redlock(
        [this.client],
        {
          // the expected clock drift; for more details
          // see http://redis.io/topics/distlock
          driftFactor: 0.1, // time in ms
          // the max number of times Redlock will attempt
          // to lock a resource before throwing error
          retryCount: 0,
          // the time in ms between attempts
          retryDelay: 200, // not needed because we are not retrying
          // time in ms
          // the max time in ms randomly added to retries
          // to improve performance under high contention
          // see https://www.awsarchitectureblog.com/2015/03/backoff.html
          retryJitter: 200, // time in ms
        },
    );

    if (config) {
      this.config = config;
      this.config.forEach((_cfg: any) => {
        _cfg['route'] = pathToRegexp(_cfg.path, undefined, {'end': false});
      });
    }
  }

  /**
   * Get status of redis client
   *
   * @return {IoRedisStatus}
   */
  get status(): IoRedisStatus {
    return this.client.status as IoRedisStatus;
  }

  /**
   * Get status of a subscriber or broker client.
   *
   * @param {'client'|'subscriber'|'brokerClient'} clientType - Client to get status of.
   * @return {IoRedisStatus}
   */
  getStatus(clientType: 'client'|'subscriber'|'brokerClient' = 'client'): IoRedisStatus {
    return this[clientType].status as IoRedisStatus;
  }

  /**
   * Fetches value from redis by redis key
   * @param {string} key
   * @return {Promise<null|any>}
   */
  async get(key: string): Promise<null|any> {
    const value = await this.client.get(key);
    return value === null ? null : JSON.parse(value);
  }

  /**
   * Sets redis value with expiration
   * @param {string} key
   * @param {any} value
   * @param {number} duration Key expire time in minutes
   * @return {Promise<string>}
   */
  async set(key: string, value: any, duration: number = 60): Promise<'OK' | null> {
    const valueStr = JSON.stringify(value);
    return await this.client.set(key, valueStr, 'EX', duration * 60);
  }

  /**
   * Fetches value from redis by redis key without json parsing
   * @param {string} key
   * @return {Promise<null|any>}
   */
  async getRaw(key: string): Promise<null|string> {
    return await this.client.get(key);
  }

  /**
   * Sets redis value with expiration without json serialization
   * @param {string} key
   * @param {any} value
   * @param {number} duration Key expire time in minutes
   * @return {Promise<string>}
   */
  async setRaw(key: string, value: string, duration: number = 60): Promise<'OK' | null> {
    return await this.client.set(key, value, 'EX', duration * 60);
  }

  /**
   * Set redis key expiry
   * @param {string} key
   * @param {number} duration Key expire time in minutes
   * @return {Promise<boolean>} `true` if expiry was set, and `false` otherwise (e.g. key not found)
   */
  async setExpiry(key: string, duration: number): Promise<boolean> {
    return Boolean(await this.client.expire(key, duration * 60));
  }

  /**
   * Increments number stored at redis key by number specified in `incrementBy` parameter
   * @param {string} key
   * @param {number} incrementBy
   * @return {Promise<number>}
   */
  async increment(key: string, incrementBy: number = 1): Promise<number> {
    return await this.client.incrby(key, incrementBy);
  }

  /**
   * Decrements number stored at redis key by number specified in `decrementBy` parameter
   * @param {string} key
   * @param {number} decrementBy
   * @return {Promise<number>}
   */
  async decrement(key: string, decrementBy: number = 1): Promise<number> {
    return await this.client.decrby(key, decrementBy);
  }

  /**
   * Check if one or more keys exist.
   *
   * Returns number of keys matched.
   * @param {string} keys
   */
  async exists(keys: string|string[]): Promise<number> {
    if (Array.isArray(keys)) {
      return await this.client.exists(...keys);
    } else {
      return await this.client.exists(keys);
    }
  }

  /**
   * Get an object of keys/values for provided keys
   * @param {string[]} keys
   */
  async getMultipleValuesWithKeys(keys: string[]): Promise<{[key: string]: string|null}> {
    if (keys.length === 0) {
      return {};
    }
    const values = await this.client.mget(keys);

    // fill with for loop due to performance
    const result: {[key: string]: string|null} = {};

    for (let i = 0; i < keys.length; i++) {
      result[keys[i]] = values[i];
    }

    return result;
  }

  /**
   * Returns an array of values for provided keys
   * @param {string[]} keys
   * @param {boolean} removeNonExistent
   * @return {Promise<any[]>}
   */
  async getMultipleValuesByKeys(keys: string[], removeNonExistent: boolean = true): Promise<any[]> {
    if (keys.length === 0) {
      return [];
    }
    const values: any[] = await this.client.mget(keys);

    return values.flatMap((item) => {
      if (removeNonExistent) {
        // removing nulls, undefined values and empty strings if there are any
        return item ? JSON.parse(item) : [];
      } else {
        // keep empty values
        return item ? JSON.parse(item) : null;
      }
    });
  }

  /**
   * Returns an object of redis key-value pairs in which keys match provided prefix
   * @param {string} prefix
   * @return {object} Redis key-value pairs
   */
  async getMultipleKeysValuesByKeyPrefix(prefix: string) {
    const keys = await this.getRedisKeys(prefix);
    const keysWithoutPrefix = keys.map((key) => this.trimPrefix(key));
    const values = await this.getMultipleValuesByKeys(keysWithoutPrefix, false);
    // return object with keys
    const keysValues: any = {};
    for (const [i, v] of values.entries()) {
      if (v) {
        keysValues[keys[i]] = v;
      }
    }
    return keysValues;
  }

  /**
   * Gets cached value from redis. If it is not set, then the parameter function is being executed
   * The function result is then stored in redis and returned.
   * @param {function} fn
   * @param {string} key
   * @param {number} expiresInMinutes
   * @return {Promise<any>}
   */
  async getCached(fn: Function, key: string, expiresInMinutes: number = 60): Promise<any> {
    const cached = await this.get(key);
    if (cached !== null) {
      return cached;
    } else {
      const result = await fn();
      await this.set(key, result, expiresInMinutes);
      return result;
    }
  }

  /**
   * Returns redis keys with given prefix.
   * @param {string} prefix Optional filter
   * @return {Promise<string[]>} keys
   */
  async getRedisKeys(prefix: string): Promise<string[]> {
    const stream = await this.client.scanStream({
      match: `${this.prefix}${prefix}*`,
      count: 1000,
    });
    const keys: Set<string> = new Set();
    return new Promise((resolve, reject) => {
      stream.on('data', (resultKeys: any) => {
        for (const key of resultKeys) {
          keys.add(key);
        }
      });

      stream.on('end', () => {
        resolve(Array.from(keys));
      });

      stream.on('error', (err: Error) => {
        reject(err);
      });
    });
  }

  /**
   * Subscribes a redis client to a channel and defines subscriber message handler
   * @param {string} channel Channel name
   * @param {function} callback Message handling function
   */
  async subscribe(channel: string, callback: (...args: any) => void): Promise<void> {
    await this.subscriber.subscribe(channel);
    this.subscriber.on('message', callback);
  }

  /**
   * Initializes and returns a stream broker
   * @typedef {object} StreamChannelBroker
   * @param {string} channel
   * @return {StreamChannelBroker}
   */
  createStreamBroker(channel: string): StreamChannelBroker {
    return new StreamChannelBroker(this.brokerClient, channel);
  }

  /**
   * Creates a consumer group to receive payload
   * Registers a new consumer with Name and Callback for message handling
   * @param {StreamChannelBroker} broker Redis stream broker
   * @param {string} name Consumer group name
   * @param {function} handler Message handling function
   * @return {Promise<void>}
   */
  async createConsumerGroup(broker: StreamChannelBroker, name: string, handler: Function): Promise<void> {
    // Creates a consumer group to receive payload
    const consumerGroup = await broker.joinConsumerGroup(name, '$');
    // Registers a new consumer with Name and Callback for message handling.
    await consumerGroup.subscribe(`Consumer-${uuid()}`, async (payloads: Payload[]) => {
      for (const element of payloads) {
        try {
          await handler(element.payload);
          await element.markAsRead();
        } catch (exception) {
          // nothing to do
        }
      }
      return 1;
    });
  }

  /**
   * Deletes one or more redis keys
   * @param {string|string[]} keys
   * @return {Promise<number>}
   */
  async delete(...keys: string[]): Promise<number> {
    if (keys.length === 0) {
      return 1;
    }
    return this.client.del(keys);
  }

  /**
   * Deletes all redis key with provided prefix
   * @param {string} prefix
   * @return {Promise<number>} Number of deleted keys
   */
  async deleteWithPrefix(prefix: string): Promise<number> {
    let keysDeleted: number = 0;
    let keysToDelete: string[] = [];
    const redisKeys: string[] = await this.getRedisKeys(prefix);
    for (const key of redisKeys) {
      keysToDelete.push(this.trimPrefix(key));
      if (keysToDelete.length >= 100) {
        keysDeleted += keysToDelete.length;
        await this.delete(...keysToDelete);
        keysToDelete = [];
      }
    }
    keysDeleted += keysToDelete.length;
    await this.delete(...keysToDelete);
    return keysDeleted;
  }

  /**
   * Removes key prefix
   * @param {string} key
   * @return {string}
   */
  trimPrefix(key: string): string {
    return key.replace(this.prefixPattern, '');
  }

  /**
   * Flushes redis keys with the prefix defined upon redis client initialization
   * @return {Promise<void>}
   */
  async flush(): Promise<void> {
    const keysDeduplicated = new Set<string>();

    const stream = await this.client.scanStream({
      match: `${this.prefix}*`,
    });
    stream.on('data', (resultKeys) => {
      for (let i = 0; i < resultKeys.length; i++) {
        keysDeduplicated.add(this.trimPrefix(resultKeys[i]));
      }
    });

    stream.on('end', async () => {
      return await this.delete(...keysDeduplicated);
    });
  }

  /**
   * Publishes message to redis stream channel
   * @param {string} channel
   * @param {any} message
   * @return {Promise<void>}
   */
  async publishMessage(channel: string, message: string): Promise<void> {
    await this.client.publish(channel, message);
  }

  /**
   * Locks the key (resource) in redis for specified amount of time (`ttl`) in ms.
   * Useful when you need to lock resources to prevent its modifications while working on it
   * @typedef {Object} Lock
   * @param {string} key
   * @param {number} ttl
   * @return {Promise<Lock>}
   */
  lockResource(key: string, ttl: number): Promise<Lock> {
    return this.redlock.acquire([`locks:${key}`], ttl);
  }

  /**
   * Functions the same as lockResource, but in case it fails to lock the resource,
   * it will retry up to `retryDuration` ms.
   * @param {string} key
   * @param {number} ttl
   * @param {number} retryDuration
   * @return {Promise<Lock>}
   */
  async lockResourceWithRetries(key: string, ttl: number, retryDuration: number): Promise<Lock> {
    const startTime = Date.now();
    // eslint-disable-next-line no-constant-condition
    while (true) {
      try {
        return await this.redlock.acquire([`locks:${key}`], ttl);
      } catch (err) {
        if (Date.now() - startTime >= retryDuration) {
          // retries expired
          throw err;
        }
      }
    }
  }

  /**
   * Retrieves cache timeout for provided endpoints
   * @param {Object} cacheParams
   * @return {boolean|number}
   */
  getCacheDuration(cacheParams: any): boolean|number {
    const {
      url,
      method,
    } = cacheParams;

    const _endpointConfig = this.config.find((api: any) => {
      return api.route.test(url);
    });
    if (
      _endpointConfig &&
        _endpointConfig.methods.includes(method.toLowerCase())
    ) {
      return _endpointConfig.cacheTimeout;
    } else {
      return false;
    }
  }

  /**
   * Stores value to redis, default timeout is 1 hour
   * key needs to have format internal-apis:cache:{hash}
   * @param {string} key
   * @param {any} value
   * @param {number} duration Key expire time in minutes
   * @return {Promise<string>}
   */
  async storeCachedData(key: string, value: any, duration = 60): Promise<'OK' | null> {
    if (value.info) {
      value.info['cacheTimeoutMinutes'] = duration;
      value.info['cachedAt'] = new Date().toISOString();
    }

    return await this.set(key, value, duration);
  }

  /**
   * Closes all redis connections
   * @return {Promise<string[]>}
   */
  async close(): Promise<string[]> {
    return await Promise.all([
      this.client.quit(),
      this.brokerClient.quit(),
    ]);
  }
}


