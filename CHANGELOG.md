# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 1.4.0
### Added
- expose `exists` command

## 1.3.0
### Added
- IGPIMP-2315 - Added `status` getter for getting connection status from ioredis.
  Added `getStatus` for getting connection status of subscriber/brokerClient clients.

## 1.2.0
### Added
- IGPIMP-2298
  - Added getMultipleValuesWithKeys - performant mget with keys including missing values
  - Added setRaw, getRaw - set/get equivalent without (de)serialization
  - Updated dependencies, tsconfig, types

## 1.1.1
### Optimized
- Improved performance of `getRedisKeys` method

## 1.1.0
### Added
- Option to set expiry on a key. Additional options are not supported.

## 1.0.1 - 08/02/2022
### Added
- Added CHANGELOG.md file
- SCT-2267 - Added `readFrom` parameter when creating consumer group

## 1.0.0 - 21/01/2022
### Added
- Added Typescript
- Added core functionalities
### Changed
- BREAKING: This package uses `redlock` v5. To release acquired lock, please use `release()` method instead of `unlock()`
    that you were supposed to use in redlock v4
