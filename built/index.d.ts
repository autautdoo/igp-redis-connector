import { Redis } from 'ioredis';
import { StreamChannelBroker } from 'redis-streams-broker';
import Redlock, { Lock } from 'redlock';
declare type IoRedisStatus = 'wait' | 'reconnecting' | 'connecting' | 'connect' | 'ready' | 'close' | 'end';
declare type RedisConstructor = {
    host: string;
    port: number;
    prefix: string;
    config?: any;
};
export declare class RedisConnector {
    prefix: string;
    prefixPattern: RegExp;
    config: any;
    client: Redis;
    brokerClient: Redis;
    subscriber: Redis;
    redlock: Redlock;
    constructor({ host, port, prefix, config }: RedisConstructor);
    /**
     * Get status of redis client
     *
     * @return {IoRedisStatus}
     */
    get status(): IoRedisStatus;
    /**
     * Get status of a subscriber or broker client.
     *
     * @param {'client'|'subscriber'|'brokerClient'} clientType - Client to get status of.
     * @return {IoRedisStatus}
     */
    getStatus(clientType?: 'client' | 'subscriber' | 'brokerClient'): IoRedisStatus;
    /**
     * Fetches value from redis by redis key
     * @param {string} key
     * @return {Promise<null|any>}
     */
    get(key: string): Promise<null | any>;
    /**
     * Sets redis value with expiration
     * @param {string} key
     * @param {any} value
     * @param {number} duration Key expire time in minutes
     * @return {Promise<string>}
     */
    set(key: string, value: any, duration?: number): Promise<'OK' | null>;
    /**
     * Fetches value from redis by redis key without json parsing
     * @param {string} key
     * @return {Promise<null|any>}
     */
    getRaw(key: string): Promise<null | string>;
    /**
     * Sets redis value with expiration without json serialization
     * @param {string} key
     * @param {any} value
     * @param {number} duration Key expire time in minutes
     * @return {Promise<string>}
     */
    setRaw(key: string, value: string, duration?: number): Promise<'OK' | null>;
    /**
     * Set redis key expiry
     * @param {string} key
     * @param {number} duration Key expire time in minutes
     * @return {Promise<boolean>} `true` if expiry was set, and `false` otherwise (e.g. key not found)
     */
    setExpiry(key: string, duration: number): Promise<boolean>;
    /**
     * Increments number stored at redis key by number specified in `incrementBy` parameter
     * @param {string} key
     * @param {number} incrementBy
     * @return {Promise<number>}
     */
    increment(key: string, incrementBy?: number): Promise<number>;
    /**
     * Decrements number stored at redis key by number specified in `decrementBy` parameter
     * @param {string} key
     * @param {number} decrementBy
     * @return {Promise<number>}
     */
    decrement(key: string, decrementBy?: number): Promise<number>;
    /**
     * Check if one or more keys exist.
     *
     * Returns number of keys matched.
     * @param {string} keys
     */
    exists(keys: string | string[]): Promise<number>;
    /**
     * Get an object of keys/values for provided keys
     * @param {string[]} keys
     */
    getMultipleValuesWithKeys(keys: string[]): Promise<{
        [key: string]: string | null;
    }>;
    /**
     * Returns an array of values for provided keys
     * @param {string[]} keys
     * @param {boolean} removeNonExistent
     * @return {Promise<any[]>}
     */
    getMultipleValuesByKeys(keys: string[], removeNonExistent?: boolean): Promise<any[]>;
    /**
     * Returns an object of redis key-value pairs in which keys match provided prefix
     * @param {string} prefix
     * @return {object} Redis key-value pairs
     */
    getMultipleKeysValuesByKeyPrefix(prefix: string): Promise<any>;
    /**
     * Gets cached value from redis. If it is not set, then the parameter function is being executed
     * The function result is then stored in redis and returned.
     * @param {function} fn
     * @param {string} key
     * @param {number} expiresInMinutes
     * @return {Promise<any>}
     */
    getCached(fn: Function, key: string, expiresInMinutes?: number): Promise<any>;
    /**
     * Returns redis keys with given prefix.
     * @param {string} prefix Optional filter
     * @return {Promise<string[]>} keys
     */
    getRedisKeys(prefix: string): Promise<string[]>;
    /**
     * Subscribes a redis client to a channel and defines subscriber message handler
     * @param {string} channel Channel name
     * @param {function} callback Message handling function
     */
    subscribe(channel: string, callback: (...args: any) => void): Promise<void>;
    /**
     * Initializes and returns a stream broker
     * @typedef {object} StreamChannelBroker
     * @param {string} channel
     * @return {StreamChannelBroker}
     */
    createStreamBroker(channel: string): StreamChannelBroker;
    /**
     * Creates a consumer group to receive payload
     * Registers a new consumer with Name and Callback for message handling
     * @param {StreamChannelBroker} broker Redis stream broker
     * @param {string} name Consumer group name
     * @param {function} handler Message handling function
     * @return {Promise<void>}
     */
    createConsumerGroup(broker: StreamChannelBroker, name: string, handler: Function): Promise<void>;
    /**
     * Deletes one or more redis keys
     * @param {string|string[]} keys
     * @return {Promise<number>}
     */
    delete(...keys: string[]): Promise<number>;
    /**
     * Deletes all redis key with provided prefix
     * @param {string} prefix
     * @return {Promise<number>} Number of deleted keys
     */
    deleteWithPrefix(prefix: string): Promise<number>;
    /**
     * Removes key prefix
     * @param {string} key
     * @return {string}
     */
    trimPrefix(key: string): string;
    /**
     * Flushes redis keys with the prefix defined upon redis client initialization
     * @return {Promise<void>}
     */
    flush(): Promise<void>;
    /**
     * Publishes message to redis stream channel
     * @param {string} channel
     * @param {any} message
     * @return {Promise<void>}
     */
    publishMessage(channel: string, message: string): Promise<void>;
    /**
     * Locks the key (resource) in redis for specified amount of time (`ttl`) in ms.
     * Useful when you need to lock resources to prevent its modifications while working on it
     * @typedef {Object} Lock
     * @param {string} key
     * @param {number} ttl
     * @return {Promise<Lock>}
     */
    lockResource(key: string, ttl: number): Promise<Lock>;
    /**
     * Functions the same as lockResource, but in case it fails to lock the resource,
     * it will retry up to `retryDuration` ms.
     * @param {string} key
     * @param {number} ttl
     * @param {number} retryDuration
     * @return {Promise<Lock>}
     */
    lockResourceWithRetries(key: string, ttl: number, retryDuration: number): Promise<Lock>;
    /**
     * Retrieves cache timeout for provided endpoints
     * @param {Object} cacheParams
     * @return {boolean|number}
     */
    getCacheDuration(cacheParams: any): boolean | number;
    /**
     * Stores value to redis, default timeout is 1 hour
     * key needs to have format internal-apis:cache:{hash}
     * @param {string} key
     * @param {any} value
     * @param {number} duration Key expire time in minutes
     * @return {Promise<string>}
     */
    storeCachedData(key: string, value: any, duration?: number): Promise<'OK' | null>;
    /**
     * Closes all redis connections
     * @return {Promise<string[]>}
     */
    close(): Promise<string[]>;
}
export {};
