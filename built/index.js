"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisConnector = void 0;
const ioredis_1 = __importDefault(require("ioredis"));
const path_to_regexp_1 = require("path-to-regexp");
const redis_streams_broker_1 = require("redis-streams-broker");
const uuid_1 = require("uuid");
const redlock_1 = __importDefault(require("redlock"));
class RedisConnector {
    prefix;
    prefixPattern;
    config;
    client;
    brokerClient;
    subscriber;
    redlock;
    constructor({ host, port, prefix, config }) {
        this.prefix = prefix;
        this.prefixPattern = new RegExp('^' + prefix);
        // ioredis connects automatically and throws on connection error
        this.client = new ioredis_1.default({ host, port, keyPrefix: prefix });
        this.subscriber = new ioredis_1.default({ host, port });
        this.brokerClient = new ioredis_1.default({ host, port });
        this.redlock = new redlock_1.default([this.client], {
            // the expected clock drift; for more details
            // see http://redis.io/topics/distlock
            driftFactor: 0.1,
            // the max number of times Redlock will attempt
            // to lock a resource before throwing error
            retryCount: 0,
            // the time in ms between attempts
            retryDelay: 200,
            // time in ms
            // the max time in ms randomly added to retries
            // to improve performance under high contention
            // see https://www.awsarchitectureblog.com/2015/03/backoff.html
            retryJitter: 200, // time in ms
        });
        if (config) {
            this.config = config;
            this.config.forEach((_cfg) => {
                _cfg['route'] = (0, path_to_regexp_1.pathToRegexp)(_cfg.path, undefined, { 'end': false });
            });
        }
    }
    /**
     * Get status of redis client
     *
     * @return {IoRedisStatus}
     */
    get status() {
        return this.client.status;
    }
    /**
     * Get status of a subscriber or broker client.
     *
     * @param {'client'|'subscriber'|'brokerClient'} clientType - Client to get status of.
     * @return {IoRedisStatus}
     */
    getStatus(clientType = 'client') {
        return this[clientType].status;
    }
    /**
     * Fetches value from redis by redis key
     * @param {string} key
     * @return {Promise<null|any>}
     */
    async get(key) {
        const value = await this.client.get(key);
        return value === null ? null : JSON.parse(value);
    }
    /**
     * Sets redis value with expiration
     * @param {string} key
     * @param {any} value
     * @param {number} duration Key expire time in minutes
     * @return {Promise<string>}
     */
    async set(key, value, duration = 60) {
        const valueStr = JSON.stringify(value);
        return await this.client.set(key, valueStr, 'EX', duration * 60);
    }
    /**
     * Fetches value from redis by redis key without json parsing
     * @param {string} key
     * @return {Promise<null|any>}
     */
    async getRaw(key) {
        return await this.client.get(key);
    }
    /**
     * Sets redis value with expiration without json serialization
     * @param {string} key
     * @param {any} value
     * @param {number} duration Key expire time in minutes
     * @return {Promise<string>}
     */
    async setRaw(key, value, duration = 60) {
        return await this.client.set(key, value, 'EX', duration * 60);
    }
    /**
     * Set redis key expiry
     * @param {string} key
     * @param {number} duration Key expire time in minutes
     * @return {Promise<boolean>} `true` if expiry was set, and `false` otherwise (e.g. key not found)
     */
    async setExpiry(key, duration) {
        return Boolean(await this.client.expire(key, duration * 60));
    }
    /**
     * Increments number stored at redis key by number specified in `incrementBy` parameter
     * @param {string} key
     * @param {number} incrementBy
     * @return {Promise<number>}
     */
    async increment(key, incrementBy = 1) {
        return await this.client.incrby(key, incrementBy);
    }
    /**
     * Decrements number stored at redis key by number specified in `decrementBy` parameter
     * @param {string} key
     * @param {number} decrementBy
     * @return {Promise<number>}
     */
    async decrement(key, decrementBy = 1) {
        return await this.client.decrby(key, decrementBy);
    }
    /**
     * Check if one or more keys exist.
     *
     * Returns number of keys matched.
     * @param {string} keys
     */
    async exists(keys) {
        if (Array.isArray(keys)) {
            return await this.client.exists(...keys);
        }
        else {
            return await this.client.exists(keys);
        }
    }
    /**
     * Get an object of keys/values for provided keys
     * @param {string[]} keys
     */
    async getMultipleValuesWithKeys(keys) {
        if (keys.length === 0) {
            return {};
        }
        const values = await this.client.mget(keys);
        // fill with for loop due to performance
        const result = {};
        for (let i = 0; i < keys.length; i++) {
            result[keys[i]] = values[i];
        }
        return result;
    }
    /**
     * Returns an array of values for provided keys
     * @param {string[]} keys
     * @param {boolean} removeNonExistent
     * @return {Promise<any[]>}
     */
    async getMultipleValuesByKeys(keys, removeNonExistent = true) {
        if (keys.length === 0) {
            return [];
        }
        const values = await this.client.mget(keys);
        return values.flatMap((item) => {
            if (removeNonExistent) {
                // removing nulls, undefined values and empty strings if there are any
                return item ? JSON.parse(item) : [];
            }
            else {
                // keep empty values
                return item ? JSON.parse(item) : null;
            }
        });
    }
    /**
     * Returns an object of redis key-value pairs in which keys match provided prefix
     * @param {string} prefix
     * @return {object} Redis key-value pairs
     */
    async getMultipleKeysValuesByKeyPrefix(prefix) {
        const keys = await this.getRedisKeys(prefix);
        const keysWithoutPrefix = keys.map((key) => this.trimPrefix(key));
        const values = await this.getMultipleValuesByKeys(keysWithoutPrefix, false);
        // return object with keys
        const keysValues = {};
        for (const [i, v] of values.entries()) {
            if (v) {
                keysValues[keys[i]] = v;
            }
        }
        return keysValues;
    }
    /**
     * Gets cached value from redis. If it is not set, then the parameter function is being executed
     * The function result is then stored in redis and returned.
     * @param {function} fn
     * @param {string} key
     * @param {number} expiresInMinutes
     * @return {Promise<any>}
     */
    async getCached(fn, key, expiresInMinutes = 60) {
        const cached = await this.get(key);
        if (cached !== null) {
            return cached;
        }
        else {
            const result = await fn();
            await this.set(key, result, expiresInMinutes);
            return result;
        }
    }
    /**
     * Returns redis keys with given prefix.
     * @param {string} prefix Optional filter
     * @return {Promise<string[]>} keys
     */
    async getRedisKeys(prefix) {
        const stream = await this.client.scanStream({
            match: `${this.prefix}${prefix}*`,
            count: 1000,
        });
        const keys = new Set();
        return new Promise((resolve, reject) => {
            stream.on('data', (resultKeys) => {
                for (const key of resultKeys) {
                    keys.add(key);
                }
            });
            stream.on('end', () => {
                resolve(Array.from(keys));
            });
            stream.on('error', (err) => {
                reject(err);
            });
        });
    }
    /**
     * Subscribes a redis client to a channel and defines subscriber message handler
     * @param {string} channel Channel name
     * @param {function} callback Message handling function
     */
    async subscribe(channel, callback) {
        await this.subscriber.subscribe(channel);
        this.subscriber.on('message', callback);
    }
    /**
     * Initializes and returns a stream broker
     * @typedef {object} StreamChannelBroker
     * @param {string} channel
     * @return {StreamChannelBroker}
     */
    createStreamBroker(channel) {
        return new redis_streams_broker_1.StreamChannelBroker(this.brokerClient, channel);
    }
    /**
     * Creates a consumer group to receive payload
     * Registers a new consumer with Name and Callback for message handling
     * @param {StreamChannelBroker} broker Redis stream broker
     * @param {string} name Consumer group name
     * @param {function} handler Message handling function
     * @return {Promise<void>}
     */
    async createConsumerGroup(broker, name, handler) {
        // Creates a consumer group to receive payload
        const consumerGroup = await broker.joinConsumerGroup(name, '$');
        // Registers a new consumer with Name and Callback for message handling.
        await consumerGroup.subscribe(`Consumer-${(0, uuid_1.v1)()}`, async (payloads) => {
            for (const element of payloads) {
                try {
                    await handler(element.payload);
                    await element.markAsRead();
                }
                catch (exception) {
                    // nothing to do
                }
            }
            return 1;
        });
    }
    /**
     * Deletes one or more redis keys
     * @param {string|string[]} keys
     * @return {Promise<number>}
     */
    async delete(...keys) {
        if (keys.length === 0) {
            return 1;
        }
        return this.client.del(keys);
    }
    /**
     * Deletes all redis key with provided prefix
     * @param {string} prefix
     * @return {Promise<number>} Number of deleted keys
     */
    async deleteWithPrefix(prefix) {
        let keysDeleted = 0;
        let keysToDelete = [];
        const redisKeys = await this.getRedisKeys(prefix);
        for (const key of redisKeys) {
            keysToDelete.push(this.trimPrefix(key));
            if (keysToDelete.length >= 100) {
                keysDeleted += keysToDelete.length;
                await this.delete(...keysToDelete);
                keysToDelete = [];
            }
        }
        keysDeleted += keysToDelete.length;
        await this.delete(...keysToDelete);
        return keysDeleted;
    }
    /**
     * Removes key prefix
     * @param {string} key
     * @return {string}
     */
    trimPrefix(key) {
        return key.replace(this.prefixPattern, '');
    }
    /**
     * Flushes redis keys with the prefix defined upon redis client initialization
     * @return {Promise<void>}
     */
    async flush() {
        const keysDeduplicated = new Set();
        const stream = await this.client.scanStream({
            match: `${this.prefix}*`,
        });
        stream.on('data', (resultKeys) => {
            for (let i = 0; i < resultKeys.length; i++) {
                keysDeduplicated.add(this.trimPrefix(resultKeys[i]));
            }
        });
        stream.on('end', async () => {
            return await this.delete(...keysDeduplicated);
        });
    }
    /**
     * Publishes message to redis stream channel
     * @param {string} channel
     * @param {any} message
     * @return {Promise<void>}
     */
    async publishMessage(channel, message) {
        await this.client.publish(channel, message);
    }
    /**
     * Locks the key (resource) in redis for specified amount of time (`ttl`) in ms.
     * Useful when you need to lock resources to prevent its modifications while working on it
     * @typedef {Object} Lock
     * @param {string} key
     * @param {number} ttl
     * @return {Promise<Lock>}
     */
    lockResource(key, ttl) {
        return this.redlock.acquire([`locks:${key}`], ttl);
    }
    /**
     * Functions the same as lockResource, but in case it fails to lock the resource,
     * it will retry up to `retryDuration` ms.
     * @param {string} key
     * @param {number} ttl
     * @param {number} retryDuration
     * @return {Promise<Lock>}
     */
    async lockResourceWithRetries(key, ttl, retryDuration) {
        const startTime = Date.now();
        // eslint-disable-next-line no-constant-condition
        while (true) {
            try {
                return await this.redlock.acquire([`locks:${key}`], ttl);
            }
            catch (err) {
                if (Date.now() - startTime >= retryDuration) {
                    // retries expired
                    throw err;
                }
            }
        }
    }
    /**
     * Retrieves cache timeout for provided endpoints
     * @param {Object} cacheParams
     * @return {boolean|number}
     */
    getCacheDuration(cacheParams) {
        const { url, method, } = cacheParams;
        const _endpointConfig = this.config.find((api) => {
            return api.route.test(url);
        });
        if (_endpointConfig &&
            _endpointConfig.methods.includes(method.toLowerCase())) {
            return _endpointConfig.cacheTimeout;
        }
        else {
            return false;
        }
    }
    /**
     * Stores value to redis, default timeout is 1 hour
     * key needs to have format internal-apis:cache:{hash}
     * @param {string} key
     * @param {any} value
     * @param {number} duration Key expire time in minutes
     * @return {Promise<string>}
     */
    async storeCachedData(key, value, duration = 60) {
        if (value.info) {
            value.info['cacheTimeoutMinutes'] = duration;
            value.info['cachedAt'] = new Date().toISOString();
        }
        return await this.set(key, value, duration);
    }
    /**
     * Closes all redis connections
     * @return {Promise<string[]>}
     */
    async close() {
        return await Promise.all([
            this.client.quit(),
            this.brokerClient.quit(),
        ]);
    }
}
exports.RedisConnector = RedisConnector;
//# sourceMappingURL=index.js.map